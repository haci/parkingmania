package com.mobaxe.parking.level1;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map3 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(4.5f, worldHeight - 13.4f), new Vector2(1, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 7.7f, 3.2f, new Vector2(-worldWidth + 35,
				worldHeight - 14f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.noelCont1, 8f, 3.5f));

		BoxProperties c2 = new BoxProperties(world, 8.7f, 4.7f, new Vector2(-worldWidth + 23.5f,
				worldHeight - 20f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.noelHouse1, 9f, 5));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(4.5f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 36f, worldHeight - 20f));
		circle2.setUserData(new CustomSprite(AssetsLoader.noelFountain, 9f, 9f));

		BoxProperties c3 = new BoxProperties(world, 6.7f, 2.7f, new Vector2(-worldWidth + 51,
				worldHeight - 14f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.noelHouse2, 7f, 5));

		BoxProperties c4 = new BoxProperties(world, 8.7f, 3.7f, new Vector2(-worldWidth + 50,
				worldHeight - 26.8f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.noelCont2, 9f, 4f));

		BoxProperties c5 = new BoxProperties(world, 4.7f, 1.7f, new Vector2(-worldWidth + 43,
				worldHeight - 20f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(3), 5f, 3));

	}
}
