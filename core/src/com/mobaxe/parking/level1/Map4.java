package com.mobaxe.parking.level1;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map4 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(15.5f, worldHeight - 13f), new Vector2(0.5f, 1f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 8.7f, 3.5f, new Vector2(-worldWidth + 25,
				worldHeight - 18f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 9, 4));

		BoxProperties c2 = new BoxProperties(world, 8.7f, 4.5f, new Vector2(-worldWidth + 25,
				worldHeight - 24f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(1), 9, 5));

		BoxProperties c3 = new BoxProperties(world, 3.5f, 6.2f,
				new Vector2(-worldWidth + 37, worldHeight - 15f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.8f, 6.5f));

		BoxProperties c4 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 37,
				worldHeight - 20.2f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c5 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 37.3f,
				worldHeight - 22.2f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c6 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 37,
				worldHeight - 25f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c7 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 41.5f,
				worldHeight - 21.8f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c8 = new BoxProperties(world, 8.7f, 3.2f, new Vector2(-worldWidth + 49f,
				worldHeight - 27f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.container, 9, 3.5f));

		BoxProperties c9 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 47,
				worldHeight - 16f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c10 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 51.5f,
				worldHeight - 16.2f));
		c10.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(2), 4, 2));

		BoxProperties c11 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 51.5f,
				worldHeight - 18.2f));
		c11.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c12 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 28.5f,
				worldHeight - 27.8f));
		c12.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c13 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 33.5f,
				worldHeight - 13.2f));
		c13.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

	}
}
