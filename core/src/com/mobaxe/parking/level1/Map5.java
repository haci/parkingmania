package com.mobaxe.parking.level1;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map5 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(16.7f, worldHeight - 28f), new Vector2(0.5f, 1f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 6.7f, 8.7f, new Vector2(-worldWidth + 26.5f,
				worldHeight - 21f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.m5leftCars, 7, 9));

		BoxProperties c2 = new BoxProperties(world, 6.7f, 3.5f, new Vector2(-worldWidth + 45.5f,
				worldHeight - 17f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.tripleCars, 7, 3.8f));

		BoxProperties c3 = new BoxProperties(world, 6.7f, 3.5f, new Vector2(-worldWidth + 50.8f,
				worldHeight - 24.6f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.tripleCars, 7, 3.8f));

		// FOUNTAIN
		CircleShape cs = (CircleShape) Box2DFactory.createCircleShape(3.3f);
		FixtureDef fd = Box2DFactory.createFixture(cs, 1, 0.1f, 0.1f, false);
		Body circle = Box2DFactory.createBody(world, BodyType.StaticBody, fd, new Vector2(
				-worldWidth + 37.5f, worldHeight - 17.8f));
		circle.setUserData(new CustomSprite(AssetsLoader.fountain, 7f, 7f));

		BoxProperties c4 = new BoxProperties(world, 15.7f, 2.2f, new Vector2(-worldWidth + 38f,
				worldHeight - 26f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.m5hcars, 16f, 2.3f));

		BoxProperties c5 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 25,
				worldHeight - 15.4f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

	}
}
