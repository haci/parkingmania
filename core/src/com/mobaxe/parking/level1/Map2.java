package com.mobaxe.parking.level1;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map2 {

	private float spriteWidth = 11f;
	private float spriteHeight = 3f;
	private float vertSpriteWidth = 2.5f;
	private float vertSpriteHeight = 10;

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(-15.5f, worldHeight - 27f), new Vector2(1.5f, 2f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 36f,
				worldHeight - 13.5f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.map2container, spriteWidth, spriteHeight));

		BoxProperties c2 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 48f,
				worldHeight - 13.5f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.map2container, spriteWidth, spriteHeight));

		BoxProperties c3 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 32f,
				worldHeight - 20.5f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.map2container, spriteWidth, spriteHeight));

		BoxProperties c4 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 44f,
				worldHeight - 20.5f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.map2container, spriteWidth, spriteHeight));

		BoxProperties c5 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 35f,
				worldHeight - 26.5f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.map2container, spriteWidth, spriteHeight));

		BoxProperties c6 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 47f,
				worldHeight - 26.5f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.map2container, spriteWidth, spriteHeight));

		BoxProperties c7 = new BoxProperties(world, 2.7f, 10.2f, new Vector2(-worldWidth + 21f,
				worldHeight - 19.6f));
		c7.getBody().setUserData(
				new CustomSprite(AssetsLoader.map2vertContainer, vertSpriteWidth, vertSpriteHeight));
		BoxProperties c8 = new BoxProperties(world, 2.7f, 10.2f, new Vector2(-worldWidth + 24f,
				worldHeight - 19.6f));
		c8.getBody().setUserData(
				new CustomSprite(AssetsLoader.map2vertContainer, vertSpriteWidth, vertSpriteHeight));

	}
}
