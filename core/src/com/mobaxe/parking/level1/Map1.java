package com.mobaxe.parking.level1;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map1 {

	private float spriteWidth = 11f;
	private float spriteHeight = 3f;
	private float vertSpriteWidth = spriteHeight + 1;
	private float vertSpriteHeight = spriteWidth;

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(16f, worldHeight - 27f), new Vector2(1.5f, 2f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 26f,
				worldHeight - 16f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.container, spriteWidth, spriteHeight));

		BoxProperties c2 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 40f,
				worldHeight - 16f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.container, spriteWidth, spriteHeight));

		BoxProperties c3 = new BoxProperties(world, 3.7f, 10.7f, new Vector2(-worldWidth + 52f,
				worldHeight - 19.6f));
		c3.getBody().setUserData(
				new CustomSprite(AssetsLoader.vertContainer, vertSpriteWidth, vertSpriteHeight));

		BoxProperties c4 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 30f,
				worldHeight - 23.7f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.container, spriteWidth, spriteHeight));

		BoxProperties c5 = new BoxProperties(world, 10.7f, 2.7f, new Vector2(-worldWidth + 44f,
				worldHeight - 23.7f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.container, spriteWidth, spriteHeight));

	}
}
