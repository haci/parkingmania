package com.mobaxe.parking;

import com.badlogic.gdx.Game;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.screens.MyScreens;

public class ParkingMania extends Game {

	private static ParkingPreferences prefs;
	public static ActionResolver actionResolver;

	@Override
	public void create() {
		prefs = new ParkingPreferences();
		prefs.saveMapLock("" + 11, true);
		AssetsLoader.loadTexturesOnCreate();
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.SPLASH_SCREEN);
	}

	public ParkingMania(ActionResolver actionResolver) {
		ParkingMania.actionResolver = actionResolver;
	}

	public static ParkingPreferences getPrefs() {
		return prefs;
	}

	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
		AssetsLoader.dispose();
	}
}
