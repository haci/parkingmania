package com.mobaxe.parking.level3;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map12 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(15.8f, worldHeight - 28.6f), new Vector2(1f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 8.7f, 4.7f, new Vector2(-worldWidth + 33f,
				worldHeight - 14.5f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 9, 5));

		BoxProperties c2 = new BoxProperties(world, 3.2f, 6.2f, new Vector2(-worldWidth + 28f,
				worldHeight - 22f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.5f, 6.5f));

		BoxProperties c3 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 35f,
				worldHeight - 27.5f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c4 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 41,
				worldHeight - 16f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c5 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 42,
				worldHeight - 20f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c6 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 43,
				worldHeight - 14.5f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c7 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 51f,
				worldHeight - 14.5f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c8 = new BoxProperties(world, 3.2f, 6.2f, new Vector2(-worldWidth + 48f,
				worldHeight - 22f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.5f, 6.5f));

		BoxProperties c9 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 51,
				worldHeight - 27f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c10 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 45.5f,
				worldHeight - 27f));
		c10.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c11 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 43.5f,
				worldHeight - 27f));
		c11.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c12 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 41.5f,
				worldHeight - 27f));
		c12.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c13 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 39.5f,
				worldHeight - 27f));
		c13.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(3f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 36f, worldHeight - 23f));
		circle2.setUserData(new CustomSprite(AssetsLoader.fountain, 6, 6));

	}
}
