package com.mobaxe.parking.level3;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map11 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(7.6f, worldHeight - 13.2f), new Vector2(1f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 8.7f, 4.7f, new Vector2(-worldWidth + 24f,
				worldHeight - 21f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 9, 5));

		BoxProperties c2 = new BoxProperties(world, 6.2f, 3.7f, new Vector2(-worldWidth + 32f,
				worldHeight - 14f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(3), 6.5f, 4));

		BoxProperties c3 = new BoxProperties(world, 6.2f, 3.3f, new Vector2(-worldWidth + 32f,
				worldHeight - 27.5f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(3), 6.5f, 4));

		BoxProperties c4 = new BoxProperties(world, 3.2f, 6.2f, new Vector2(-worldWidth + 34f,
				worldHeight - 20f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.5f, 6.5f));

		BoxProperties c5 = new BoxProperties(world, 3.2f, 6.2f, new Vector2(-worldWidth + 34f,
				worldHeight - 20f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.5f, 6.5f));

		BoxProperties c6 = new BoxProperties(world, 5.7f, 3.7f, new Vector2(-worldWidth + 38.5f,
				worldHeight - 21.5f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(1), 6, 4));

		BoxProperties c7 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 52.8f,
				worldHeight - 20f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c9 = new BoxProperties(world, 7.7f, 3.7f, new Vector2(-worldWidth + 49.8f,
				worldHeight - 15f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.tripleCars, 8, 4));

		BoxProperties c10 = new BoxProperties(world, 4.7f, 3.7f, new Vector2(-worldWidth + 39.5f,
				worldHeight - 15f));
		c10.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 5, 4));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(3.8f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 46f, worldHeight - 23f));
		circle2.setUserData(new CustomSprite(AssetsLoader.fountain, 7.5f, 7.5f));

	}
}
