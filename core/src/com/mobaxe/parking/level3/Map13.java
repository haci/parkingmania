package com.mobaxe.parking.level3;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map13 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(12f, worldHeight - 13f), new Vector2(1f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 8.7f, 4.7f, new Vector2(-worldWidth + 40f,
				worldHeight - 14.5f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 9, 5));

		BoxProperties c2 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 52,
				worldHeight - 14.5f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c3 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 46,
				worldHeight - 14.5f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c4 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 34,
				worldHeight - 14.5f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c5 = new BoxProperties(world, 6.2f, 3.7f, new Vector2(-worldWidth + 29.5f,
				worldHeight - 14f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(1), 6.5f, 4));

		BoxProperties c6 = new BoxProperties(world, 4.7f, 3.7f, new Vector2(-worldWidth + 24.5f,
				worldHeight - 17f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 5, 4));

		BoxProperties c7 = new BoxProperties(world, 4.7f, 3.7f, new Vector2(-worldWidth + 24.5f,
				worldHeight - 22f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 5, 4));

		BoxProperties c8 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 24.5f,
				worldHeight - 25));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c9 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 30f,
				worldHeight - 27f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c10 = new BoxProperties(world, 3.7f, 7.7f, new Vector2(-worldWidth + 33f,
				worldHeight - 25f));
		c10.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 4, 8f));

		BoxProperties c11 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 32f,
				worldHeight - 20));
		c11.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c12 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 38.5f,
				worldHeight - 20.5f));
		c12.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c13 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 38.5f,
				worldHeight - 24.5f));
		c13.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c14 = new BoxProperties(world, 3.7f, 7.7f, new Vector2(-worldWidth + 42f,
				worldHeight - 22f));
		c14.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 4, 8f));

		BoxProperties c15 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 45f,
				worldHeight - 24));
		c15.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c16 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 47.5f,
				worldHeight - 24));
		c16.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c17 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 45f,
				worldHeight - 20));
		c17.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c18 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 47.5f,
				worldHeight - 20));
		c18.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c19 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 52.5f,
				worldHeight - 20));
		c19.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c20 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 52.5f,
				worldHeight - 24));
		c20.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

	}
}
