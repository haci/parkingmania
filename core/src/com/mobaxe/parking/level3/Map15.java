package com.mobaxe.parking.level3;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map15 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(15.5f, worldHeight - 28f), new Vector2(1f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 10.7f, 0.7f, new Vector2(-worldWidth + 25f,
				worldHeight - 15.2f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(0), 11, 0.8f));

		BoxProperties c2 = new BoxProperties(world, 14.7f, 0.7f, new Vector2(-worldWidth + 27f,
				worldHeight - 22.5f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(0), 15, 0.8f));

		BoxProperties c3 = new BoxProperties(world, 7.7f, 4.7f, new Vector2(-worldWidth + 26.5f,
				worldHeight - 18.75f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 8, 5));

		BoxProperties c4 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 32,
				worldHeight - 19.5f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c5 = new BoxProperties(world, 0.7f, 2.7f, new Vector2(-worldWidth + 34f,
				worldHeight - 13.2f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(1), 0.8f, 3));

		BoxProperties c6 = new BoxProperties(world, 0.7f, 3.2f, new Vector2(-worldWidth + 34f,
				worldHeight - 20.3f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(1), 0.8f, 3.5f));

		BoxProperties c7 = new BoxProperties(world, 14.7f, 0.7f, new Vector2(-worldWidth + 31f,
				worldHeight - 26f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(0), 15, 0.8f));

		BoxProperties c8 = new BoxProperties(world, 0.7f, 13.4f, new Vector2(-worldWidth + 38f,
				worldHeight - 18.7f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(3), 0.8f, 13.7f));

		BoxProperties c9 = new BoxProperties(world, 6.7f, 0.7f, new Vector2(-worldWidth + 42f,
				worldHeight - 22.3f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(2), 7, 0.8f));

		BoxProperties c10 = new BoxProperties(world, 6.7f, 0.7f, new Vector2(-worldWidth + 42f,
				worldHeight - 17.3f));
		c10.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(2), 7, 0.8f));

		BoxProperties c11 = new BoxProperties(world, 6.2f, 3.2f, new Vector2(-worldWidth + 42f,
				worldHeight - 19.8f));
		c11.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(3), 6.5f, 3.5f));

		BoxProperties c12 = new BoxProperties(world, 0.7f, 2.7f, new Vector2(-worldWidth + 42f,
				worldHeight - 27.7f));
		c12.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(1), 0.8f, 3));

		BoxProperties c13 = new BoxProperties(world, 8.2f, 0.7f, new Vector2(-worldWidth + 45.7f,
				worldHeight - 25.8f));
		c13.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(2), 8.5f, 0.8f));

		BoxProperties c14 = new BoxProperties(world, 6.2f, 3.2f, new Vector2(-worldWidth + 46f,
				worldHeight - 27.7f));
		c14.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(3), 6.5f, 3.5f));

		BoxProperties c15 = new BoxProperties(world, 0.7f, 11.7f, new Vector2(-worldWidth + 49.4f,
				worldHeight - 20f));
		c15.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(3), 0.8f, 12f));

		BoxProperties c16 = new BoxProperties(world, 7.2f, 0.7f, new Vector2(-worldWidth + 46f,
				worldHeight - 14.2f));
		c16.getBody().setUserData(new CustomSprite(AssetsLoader.walls.get(2), 7.5f, 0.8f));

		BoxProperties c17 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 50.7f,
				worldHeight - 16.5f));
		c17.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c18 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 53.7f,
				worldHeight - 22f));
		c18.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c19 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 50.7f,
				worldHeight - 27.5f));
		c19.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

	}
}
