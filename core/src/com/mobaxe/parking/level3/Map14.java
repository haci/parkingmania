package com.mobaxe.parking.level3;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map14 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(17f, worldHeight - 28f), new Vector2(1f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 8.7f, 4.7f, new Vector2(-worldWidth + 24f,
				worldHeight - 20));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 9, 5));

		BoxProperties c2 = new BoxProperties(world, 3.7f, 6.2f, new Vector2(-worldWidth + 34,
				worldHeight - 15.5f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(4), 4, 6.5f));

		BoxProperties c3 = new BoxProperties(world, 3.2f, 6.7f, new Vector2(-worldWidth + 33f,
				worldHeight - 23));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.5f, 7f));

		BoxProperties c4 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 29.5f,
				worldHeight - 25.8f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c5 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 25.5f,
				worldHeight - 25.8f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(2), 4, 2));

		BoxProperties c6 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 27f,
				worldHeight - 16.5f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c7 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 38.5f,
				worldHeight - 19f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(2), 4, 2));

		BoxProperties c8 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 43f,
				worldHeight - 19f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c9 = new BoxProperties(world, 3.2f, 6.7f, new Vector2(-worldWidth + 46.5f,
				worldHeight - 23f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.5f, 7f));

		@SuppressWarnings("unused")
		BoxProperties c10 = new BoxProperties(world, 10.7f, 0.7f, new Vector2(-worldWidth + 46f,
				worldHeight - 15f));
		@SuppressWarnings("unused")
		BoxProperties c11 = new BoxProperties(world, 0.7f, 12.7f, new Vector2(-worldWidth + 51.5f,
				worldHeight - 22f));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(2.5f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 40f, worldHeight - 25.8f));
		circle2.setUserData(new CustomSprite(AssetsLoader.fountain, 5, 5));

	}
}
