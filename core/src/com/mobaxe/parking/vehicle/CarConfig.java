package com.mobaxe.parking.vehicle;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.CustomSprite;

public class CarConfig {

	public final static float carWidth = 1.2f;
	public final static float carLength = 3f;
	public final static Vector2 carPosition = new Vector2(-GameManager.camera.viewportWidth + 21.5f,
			GameManager.camera.viewportHeight - 13.2f);
	public final static float carAngle = (float) Math.PI;
	public final static float carPower = 35;
	public final static float carMaxSteerAngle = 28;
	public final static float carMaxSpeed = 45;
	public final static CustomSprite carSprite = new CustomSprite(AssetsLoader.carTexture, 1.9f, 4f);
	public static Body carBody;

}
