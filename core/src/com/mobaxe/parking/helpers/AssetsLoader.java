package com.mobaxe.parking.helpers;

import java.util.ArrayList;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class AssetsLoader {

	private static FreeTypeFontGenerator generator;
	private static FreeTypeFontParameter parameter;
	public static BitmapFont font;

	private static Sound sound;
	private static Music music;
	public static ArrayList<Texture> maps = new ArrayList<Texture>();
	public static ArrayList<Texture> levels = new ArrayList<Texture>();
	public static ArrayList<Texture> cars = new ArrayList<Texture>();
	public static ArrayList<Texture> houses = new ArrayList<Texture>();
	public static ArrayList<Texture> walls = new ArrayList<Texture>();
	public static Texture doubleCars, tripleCars;
	public static Texture fountain;
	public static Texture m6car2;
	public static Texture m5hcars, m5leftCars;
	public static Texture screenBg;
	public static Texture container, vertContainer;
	public static Texture map2container, map2vertContainer;
	public static Texture noelHouse1, noelHouse2, noelCont1, noelCont2, noelFountain;
	public static Texture carTexture, gas, gasOn, brake, brakeOn, steeringWheel;
	public static Texture splash, crash;

	public static Music carfireSound = loadMusic("sounds/carstart.mp3");
	public static Music carCrashSound = loadMusic("sounds/carcrash.mp3");
	public static Music gamePlaySound = loadMusic("sounds/gameplay.mp3");

	public static Texture loadTexture(String filePath) {
		return new Texture(Gdx.files.internal(filePath));
	}

	public static Sound loadSound(String filePath) {
		sound = Gdx.audio.newSound(Gdx.files.internal(filePath));
		return sound;
	}

	public static Music loadMusic(String filePath) {
		music = Gdx.audio.newMusic(Gdx.files.internal(filePath));
		return music;
	}

	public static void loadTexturesOnCreate() {
		// MAP 1
		container = loadTexture("maps/map1/container.png");
		vertContainer = loadTexture("maps/map1/verticalContainer.png");
		// MAP 2
		map2container = loadTexture("maps/map2/map2container.png");
		map2vertContainer = loadTexture("maps/map2/map2verticalContainer.png");
		// MAP 3
		noelHouse1 = loadTexture("maps/map3/noelhouse1.png");
		noelHouse2 = loadTexture("maps/map3/noelhouse2.png");
		noelCont1 = loadTexture("maps/map3/noelcont.png");
		noelCont2 = loadTexture("maps/map3/noelcont2.png");
		noelFountain = loadTexture("maps/map3/noel.png");
		// MAP 5
		m5hcars = loadTexture("maps/map5/hcar.png");
		m5leftCars = loadTexture("maps/map5/leftcar.png");
		// MAP 6
		m6car2 = loadTexture("maps/map6/car2.png");
		// VEHICLE
		carTexture = loadTexture("vehicle/car.png");
		gas = loadTexture("vehicle/gas.png");
		gasOn = loadTexture("vehicle/gas_on.png");
		brake = loadTexture("vehicle/brake.png");
		brakeOn = loadTexture("vehicle/brake_on.png");
		steeringWheel = loadTexture("vehicle/swheel.png");
		// SHARED IMAGES
		// HOUSES
		if (houses.size() > 0) {
			houses.clear();
		}
		houses.add(loadTexture("sharedImages/house1.png"));
		houses.add(loadTexture("sharedImages/house2.png"));
		houses.add(loadTexture("sharedImages/house3.png"));
		houses.add(loadTexture("sharedImages/house4.png"));
		houses.add(loadTexture("sharedImages/house5.png"));
		fountain = loadTexture("sharedImages/fountain.png");
		tripleCars = loadTexture("sharedImages/triplecars.png");
		doubleCars = loadTexture("sharedImages/doublecars.png");
		// CARS
		if (cars.size() > 0) {
			cars.clear();
		}
		cars.add(loadTexture("sharedImages/car1.png"));
		cars.add(loadTexture("sharedImages/car2.png"));
		cars.add(loadTexture("sharedImages/car3.png"));
		cars.add(loadTexture("sharedImages/car4.png"));
		cars.add(loadTexture("sharedImages/car5.png"));

		// WALLS
		if (walls.size() > 0) {
			walls.clear();
		}
		walls.add(loadTexture("maps/map15/wall1.png"));
		walls.add(loadTexture("maps/map15/wall2.png"));
		walls.add(loadTexture("maps/map15/wall3.png"));
		walls.add(loadTexture("maps/map15/wall4.png"));
		walls.add(loadTexture("maps/map15/wall5.png"));
		// LEVELS
		if (levels.size() > 0) {
			levels.clear();
		}
		levels.add(loadTexture("stage/1.png"));
		levels.add(loadTexture("stage/2.png"));
		levels.add(loadTexture("stage/3.png"));
		levels.add(loadTexture("stage/4.png"));
		levels.add(loadTexture("stage/5.png"));
		// STAGE
		screenBg = loadTexture("stage/bg.jpg");
		// MAPS
		if (maps.size() > 0) {
			maps.clear();
		}
		maps.add(loadTexture("maps/map1/map1.jpg"));
		maps.add(loadTexture("maps/map2/map2.jpg"));
		maps.add(loadTexture("maps/map3/map3.jpg"));
		maps.add(loadTexture("maps/map4/map4.jpg"));
		maps.add(loadTexture("maps/map5/map5.jpg"));
		maps.add(loadTexture("maps/map6/map6.jpg"));
		maps.add(loadTexture("maps/map7/map7.jpg"));
		maps.add(loadTexture("maps/map8/map8.jpg"));
		maps.add(loadTexture("maps/map9/map9.jpg"));
		maps.add(loadTexture("maps/map10/map10.jpg"));
		maps.add(loadTexture("maps/map11/map11.jpg"));
		maps.add(loadTexture("maps/map12/map12.jpg"));
		maps.add(loadTexture("maps/map13/map13.jpg"));
		maps.add(loadTexture("maps/map14/map14.jpg"));
		maps.add(loadTexture("maps/map15/map15.jpg"));

		// LOGO

		splash = loadTexture("logo.png");
		crash = loadTexture("crashed.jpg");

	}

	public static void generateFont() {
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/text.ttf"));
		parameter = new FreeTypeFontParameter();
		int density = 50;
		if (ApplicationType.Android == Gdx.app.getType()) {
			density = 15;
		}
		parameter.size = (int) (density * Gdx.graphics.getDensity());
		font = generator.generateFont(parameter);

		generator.dispose(); // don't forget to dispose to avoid memory leaks!

	}

	public static void dispose() {
		walls.get(0).dispose();
		walls.get(1).dispose();
		walls.get(2).dispose();
		walls.get(3).dispose();
		walls.get(4).dispose();
		maps.get(0).dispose();
		maps.get(1).dispose();
		maps.get(2).dispose();
		maps.get(3).dispose();
		maps.get(4).dispose();
		maps.get(5).dispose();
		maps.get(6).dispose();
		maps.get(7).dispose();
		maps.get(8).dispose();
		maps.get(9).dispose();
		maps.get(10).dispose();
		maps.get(11).dispose();
		maps.get(12).dispose();
		maps.get(13).dispose();
		maps.get(14).dispose();
		houses.get(0).dispose();
		houses.get(1).dispose();
		houses.get(2).dispose();
		houses.get(3).dispose();
		houses.get(4).dispose();
		cars.get(0).dispose();
		cars.get(1).dispose();
		cars.get(2).dispose();
		cars.get(3).dispose();
		cars.get(4).dispose();
		noelHouse1.dispose();
		noelHouse2.dispose();
		noelCont1.dispose();
		noelCont2.dispose();
		noelFountain.dispose();
		doubleCars.dispose();
		m6car2.dispose();
		fountain.dispose();
		m5hcars.dispose();
		m5leftCars.dispose();
		tripleCars.dispose();
		container.dispose();
		vertContainer.dispose();
		splash.dispose();
		// carTexture.dispose();
		gas.dispose();
		gasOn.dispose();
		brake.dispose();
		brakeOn.dispose();
		steeringWheel.dispose();
		map2container.dispose();
		map2vertContainer.dispose();
		screenBg.dispose();
		crash.dispose();
	}
}