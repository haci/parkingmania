package com.mobaxe.parking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class ParkingPreferences {

	private Preferences prefs = Gdx.app.getPreferences("parking");

	public void saveMapLock(String key, boolean shouldUnlock) {
		prefs.putBoolean(key, shouldUnlock);
		prefs.flush();
	}

	public boolean getMapLock(String key) {
		return prefs.getBoolean(key);
	}

	public void clearPrefs() {
		prefs.clear();
	}

}
