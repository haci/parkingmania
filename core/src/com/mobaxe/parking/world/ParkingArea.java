package com.mobaxe.parking.world;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class ParkingArea {

	public static void create(World world, Vector2 position, Vector2 size) {
		BoxProperties parkArea = new BoxProperties(world, size.x, size.y, position);
		parkArea.getBody().getFixtureList().get(0).setSensor(true);

	}
}
