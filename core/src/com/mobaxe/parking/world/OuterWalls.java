package com.mobaxe.parking.world;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class OuterWalls {

	@SuppressWarnings("unused")
	public void createOuterWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		BoxProperties leftWall = new BoxProperties(world, 0.5f, worldHeight, new Vector2(-worldWidth + 19, 0));
		BoxProperties rightWall = new BoxProperties(world, 0.5f, worldHeight, new Vector2(worldWidth - 19, 0));
		BoxProperties bottomWall = new BoxProperties(world, worldWidth, 0.5f, new Vector2(0,
				-worldHeight + 14.5f));
		BoxProperties topWall = new BoxProperties(world, worldWidth, 0.5f,
				new Vector2(0, worldHeight - 11.5f));
	}
}
