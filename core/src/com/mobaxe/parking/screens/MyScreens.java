package com.mobaxe.parking.screens;

import com.badlogic.gdx.Screen;
import com.mobaxe.parking.ParkingMania;

public enum MyScreens {

	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen(ParkingMania.actionResolver);
		}
	},
	SPLASH_SCREEN {
		public Screen getScreenInstance() {
			return new SplashScreen();
		}
	},
	CRASH_SCREEN {
		public Screen getScreenInstance() {
			return new CrashScreen();
		}
	},
	LEVEL_SCREEN {
		public Screen getScreenInstance() {
			return new StageSelectionScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
