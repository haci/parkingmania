package com.mobaxe.parking.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.parking.ScreenManager;
import com.mobaxe.parking.cons.Utils;
import com.mobaxe.parking.helpers.AssetsLoader;

public class CrashScreen extends InputAdapter implements Screen {

	private Stage stage;
	private Image bg;

	public CrashScreen() {
		bg = new Image(AssetsLoader.crash);
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act();
		stage.draw();

	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		ScreenManager.getInstance().dispose(MyScreens.CRASH_SCREEN);
		ScreenManager.getInstance().show(MyScreens.LEVEL_SCREEN);
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return true;
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(this);
		bg.setSize(50, 50);
		bg.setPosition(Utils.virtualWidth / 2f, Utils.virtualHeight / 2);
		bg.setOrigin(bg.getWidth() / 2, bg.getHeight() / 2);
		bg.addAction(Actions.scaleBy(16, 10, 0.3f, Interpolation.elasticOut));
		stage.addActor(bg);

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}