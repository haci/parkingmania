package com.mobaxe.parking.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.parking.ParkingMania;
import com.mobaxe.parking.buttons.LevelButtons;
import com.mobaxe.parking.cons.Utils;
import com.mobaxe.parking.helpers.AssetsLoader;

public class StageSelectionScreen implements Screen {

	private Stage stage;
	private Table bgTable;
	private Table firstLine;
	private Table secondLine;
	private Table thirdLine;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;

	public StageSelectionScreen() {

		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
		bgTable = new Table();
		firstLine = new Table();
		secondLine = new Table();
		thirdLine = new Table();

		if (!AssetsLoader.gamePlaySound.isPlaying()) {
			AssetsLoader.gamePlaySound.setVolume(1.5f);
			AssetsLoader.gamePlaySound.play();
		}
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {

		bgTable.setFillParent(true);
		firstLine.setFillParent(true);
		secondLine.setFillParent(true);
		thirdLine.setFillParent(true);

		// LEVEL 1
		LevelButtons l1 = new LevelButtons(1, 1);
		LevelButtons l2 = new LevelButtons(1, 2);
		if (ParkingMania.getPrefs().getMapLock(l2.getButton()) == false) {
			l2.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l3 = new LevelButtons(1, 3);
		if (ParkingMania.getPrefs().getMapLock(l3.getButton()) == false) {
			l3.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l4 = new LevelButtons(1, 4);
		if (ParkingMania.getPrefs().getMapLock(l4.getButton()) == false) {
			l4.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l5 = new LevelButtons(1, 5);
		if (ParkingMania.getPrefs().getMapLock(l5.getButton()) == false) {
			l5.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}

		firstLine.add(l1).pad(0, 0, 180, 10);
		firstLine.add(l2).pad(0, 0, 180, 10);
		firstLine.add(l3).pad(0, 0, 180, 10);
		firstLine.add(l4).pad(0, 0, 180, 10);
		firstLine.add(l5).pad(0, 0, 180, -10);

		// LEVEL 2
		LevelButtons l6 = new LevelButtons(2, 1);
		if (ParkingMania.getPrefs().getMapLock(l6.getButton()) == false) {
			l6.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l7 = new LevelButtons(2, 2);
		if (ParkingMania.getPrefs().getMapLock(l7.getButton()) == false) {
			l7.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l8 = new LevelButtons(2, 3);
		if (ParkingMania.getPrefs().getMapLock(l8.getButton()) == false) {
			l8.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l9 = new LevelButtons(2, 4);
		if (ParkingMania.getPrefs().getMapLock(l9.getButton()) == false) {
			l9.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l10 = new LevelButtons(2, 5);
		if (ParkingMania.getPrefs().getMapLock(l10.getButton()) == false) {
			l10.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}

		secondLine.add(l6).pad(0, 0, -40, 10);
		secondLine.add(l7).pad(0, 0, -40, 10);
		secondLine.add(l8).pad(0, 0, -40, 10);
		secondLine.add(l9).pad(0, 0, -40, 10);
		secondLine.add(l10).pad(0, 0, -40, -10);

		// LEVEL 3
		LevelButtons l11 = new LevelButtons(3, 1);
		if (ParkingMania.getPrefs().getMapLock(l11.getButton()) == false) {
			l11.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l12 = new LevelButtons(3, 2);
		if (ParkingMania.getPrefs().getMapLock(l12.getButton()) == false) {
			l12.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l13 = new LevelButtons(3, 3);
		if (ParkingMania.getPrefs().getMapLock(l13.getButton()) == false) {
			l13.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l14 = new LevelButtons(3, 4);
		if (ParkingMania.getPrefs().getMapLock(l14.getButton()) == false) {
			l14.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		LevelButtons l15 = new LevelButtons(3, 5);
		if (ParkingMania.getPrefs().getMapLock(l15.getButton()) == false) {
			l15.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}

		thirdLine.add(l11).pad(0, 0, -260, 10);
		thirdLine.add(l12).pad(0, 0, -260, 10);
		thirdLine.add(l13).pad(0, 0, -260, 10);
		thirdLine.add(l14).pad(0, 0, -260, 10);
		thirdLine.add(l15).pad(0, 0, -260, -10);

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);
		stage.addActor(bgTable);
		stage.addActor(firstLine);
		stage.addActor(secondLine);
		stage.addActor(thirdLine);
		stage.addAction(Actions.sequence(Actions.moveBy(-800, 0), Actions.moveTo(0, 0, 0.5f)));

	}

	private void setBackgroundImage() {
		bgTexture = AssetsLoader.screenBg;

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(Utils.virtualWidth - bgSprite.getWidth() / 2);
		bgSprite.setY(Utils.virtualHeight - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
