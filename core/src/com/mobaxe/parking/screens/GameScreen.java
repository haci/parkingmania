package com.mobaxe.parking.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.parking.ActionResolver;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.GameState;
import com.mobaxe.parking.ScreenManager;
import com.mobaxe.parking.cons.Utils;

public class GameScreen implements Screen {

	private OrthographicCamera camera;
	private World world;
	private Box2DDebugRenderer debugRenderer;
	private SpriteBatch spriteBatch;
	private Stage stage;

	public GameScreen(ActionResolver actionResolver) {
		GameManager.actionResolver = actionResolver;
	}

	@Override
	public void show() {
		world = new World(Utils.GRAVITY, true);
		camera = new OrthographicCamera(Utils.widthMeters, Utils.heightMeters);
		debugRenderer = new Box2DDebugRenderer();
		spriteBatch = new SpriteBatch();
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));

		GameManager.adIsShowed = false;
		GameManager.setGameState(GameState.PLAYING);
		GameManager.setWorld(world);
		GameManager.setCamera(camera);
		GameManager.setStage(stage);
		GameManager.resetCarVelocityEachTime();
		GameManager.createWorldContactListener();
		GameManager.createMap();
		GameManager.createCar();
		GameManager.createOuterWalls();
		GameManager.createInputs();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		debugRenderer.render(world, camera.combined);

		GameManager.updateWorld(delta);
		GameManager.drawWorldSprites(delta, spriteBatch);

		if (Gdx.input.isKeyPressed(Keys.BACK)) {
			ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
			ScreenManager.getInstance().show(MyScreens.LEVEL_SCREEN);
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
