package com.mobaxe.parking.buttons;

import com.mobaxe.parking.ScreenManager;
import com.mobaxe.parking.screens.MyScreens;

public class MyRunnable implements Runnable {

	public MyRunnable() {
	}

	@Override
	public void run() {
		goToGameScreen();
	}

	private void goToGameScreen() {
		ScreenManager.getInstance().dispose(MyScreens.LEVEL_SCREEN);
		ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
	}

}
