package com.mobaxe.parking.buttons;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.parking.GameLevel;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.ParkingMania;
import com.mobaxe.parking.helpers.AssetsLoader;

public class LevelButtons extends Button {

	public static int clickCounter;

	private String buttonUp;
	private String buttonOver;
	private Skin skin;
	private ButtonStyle style;
	private int button;
	private int level;

	private MyRunnable runnable;

	public LevelButtons(int level, int button) {
		this.level = level;
		this.button = button;
		buttonUp = "ButtonUp";
		buttonOver = "ButtonOver";
		initSkins();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();

		runnable = new MyRunnable();

		skin.add(buttonUp, AssetsLoader.levels.get(button - 1));
		skin.add(buttonOver, AssetsLoader.levels.get(button - 1));

		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						buttonFunction(event);
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction(InputEvent event) {
				if (ParkingMania.getPrefs().getMapLock(getButton()) == false) {
					event.getListenerActor().addAction(
							Actions.sequence(Actions.moveBy(10, 0), Actions.moveBy(-10, 0)));
				} else {

					setLevel();

					event.getStage().addAction(
							Actions.sequence(Actions.fadeOut(.7f, Interpolation.pow5Out),
									Actions.run(runnable)));
				}
				clickCounter = 0;

			}

		});
	}

	public void setLevel() {
		if (level == 1) {
			if (button == 1) {
				GameManager.setCurrentLevel(GameLevel.LEVEL1);
			} else if (button == 2) {
				GameManager.setCurrentLevel(GameLevel.LEVEL2);
			} else if (button == 3) {
				GameManager.setCurrentLevel(GameLevel.LEVEL3);
			} else if (button == 4) {
				GameManager.setCurrentLevel(GameLevel.LEVEL4);
			} else if (button == 5) {
				GameManager.setCurrentLevel(GameLevel.LEVEL5);
			}
		} else if (level == 2) {
			if (button == 1) {
				GameManager.setCurrentLevel(GameLevel.LEVEL6);
			} else if (button == 2) {
				GameManager.setCurrentLevel(GameLevel.LEVEL7);
			} else if (button == 3) {
				GameManager.setCurrentLevel(GameLevel.LEVEL8);
			} else if (button == 4) {
				GameManager.setCurrentLevel(GameLevel.LEVEL9);
			} else if (button == 5) {
				GameManager.setCurrentLevel(GameLevel.LEVEL10);
			}
		} else if (level == 3) {
			if (button == 1) {
				GameManager.setCurrentLevel(GameLevel.LEVEL11);
			} else if (button == 2) {
				GameManager.setCurrentLevel(GameLevel.LEVEL12);
			} else if (button == 3) {
				GameManager.setCurrentLevel(GameLevel.LEVEL13);
			} else if (button == 4) {
				GameManager.setCurrentLevel(GameLevel.LEVEL14);
			} else if (button == 5) {
				GameManager.setCurrentLevel(GameLevel.LEVEL15);
			}
		} else {
			System.err.println("LEVEL :" + level + " " + "BUTTON :" + button);
		}
	}

	public String getButton() {
		return level + "" + button;
	}
}
