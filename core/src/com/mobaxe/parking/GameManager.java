package com.mobaxe.parking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.mobaxe.parking.cons.Utils;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.level1.Map1;
import com.mobaxe.parking.level1.Map2;
import com.mobaxe.parking.level1.Map3;
import com.mobaxe.parking.level1.Map4;
import com.mobaxe.parking.level1.Map5;
import com.mobaxe.parking.level2.Map10;
import com.mobaxe.parking.level2.Map6;
import com.mobaxe.parking.level2.Map7;
import com.mobaxe.parking.level2.Map8;
import com.mobaxe.parking.level2.Map9;
import com.mobaxe.parking.level3.Map11;
import com.mobaxe.parking.level3.Map12;
import com.mobaxe.parking.level3.Map13;
import com.mobaxe.parking.level3.Map14;
import com.mobaxe.parking.level3.Map15;
import com.mobaxe.parking.screens.MyScreens;
import com.mobaxe.parking.vehicle.CarConfig;
import com.mobaxe.parking.vehicle.TopDownCar;
import com.mobaxe.parking.world.OuterWalls;

public class GameManager {

	public static OrthographicCamera camera;
	public static Stage stage;
	public static int gameLevel = GameLevel.LEVEL1;
	public static GameState gameState = GameState.PLAYING;

	private static World world;
	public static TopDownCar car;
	private static OuterWalls outerWalls;
	private static InputMultiplexer multiplexer;

	public static int adCounter = Utils.INTERSTITIAL_FREQ - 1;
	public static ActionResolver actionResolver;
	public static boolean adIsShowed = false;

	public static void setWorld(World w) {
		world = w;
	}

	public static void setCamera(OrthographicCamera c) {
		camera = c;
	}

	public static void setStage(Stage s) {
		stage = s;
	}

	public static void setGameState(GameState state) {
		gameState = state;
	}

	private static void showIntersititial() {

		// Ads every X refresh!
		if (adCounter == Utils.INTERSTITIAL_FREQ) {
			if (!adIsShowed) {
				actionResolver.showOrLoadInterstital();
				adIsShowed = true;
				adCounter--;
			}
		} else if (adCounter == 0) {
			adCounter = Utils.INTERSTITIAL_FREQ;
		} else {
			adCounter--;
		}

	}

	public static void createMap() {
		switch (gameLevel) {
		case 1:
			Map1 map1 = new Map1();
			map1.createWalls(world, camera);
			map1.setBackground(new Image(AssetsLoader.maps.get(0)));
			break;
		case 2:
			Map2 map2 = new Map2();
			map2.createWalls(world, camera);
			map2.setBackground(new Image(AssetsLoader.maps.get(1)));
			break;
		case 3:
			Map3 map3 = new Map3();
			map3.createWalls(world, camera);
			map3.setBackground(new Image(AssetsLoader.maps.get(2)));
			break;
		case 4:
			Map4 map4 = new Map4();
			map4.createWalls(world, camera);
			map4.setBackground(new Image(AssetsLoader.maps.get(3)));
			break;
		case 5:
			Map5 map5 = new Map5();
			map5.createWalls(world, camera);
			map5.setBackground(new Image(AssetsLoader.maps.get(4)));
			break;
		case 6:
			Map6 map6 = new Map6();
			map6.createWalls(world, camera);
			map6.setBackground(new Image(AssetsLoader.maps.get(5)));
			break;
		case 7:
			Map7 map7 = new Map7();
			map7.createWalls(world, camera);
			map7.setBackground(new Image(AssetsLoader.maps.get(6)));
			break;
		case 8:
			Map8 map8 = new Map8();
			map8.createWalls(world, camera);
			map8.setBackground(new Image(AssetsLoader.maps.get(7)));
			break;
		case 9:
			Map9 map9 = new Map9();
			map9.createWalls(world, camera);
			map9.setBackground(new Image(AssetsLoader.maps.get(8)));
			break;
		case 10:
			Map10 map10 = new Map10();
			map10.createWalls(world, camera);
			map10.setBackground(new Image(AssetsLoader.maps.get(9)));
			break;
		case 11:
			Map11 map11 = new Map11();
			map11.createWalls(world, camera);
			map11.setBackground(new Image(AssetsLoader.maps.get(10)));
			break;
		case 12:
			Map12 map12 = new Map12();
			map12.createWalls(world, camera);
			map12.setBackground(new Image(AssetsLoader.maps.get(11)));
			break;
		case 13:
			Map13 map13 = new Map13();
			map13.createWalls(world, camera);
			map13.setBackground(new Image(AssetsLoader.maps.get(12)));
			break;
		case 14:
			Map14 map14 = new Map14();
			map14.createWalls(world, camera);
			map14.setBackground(new Image(AssetsLoader.maps.get(13)));
			break;
		case 15:
			Map15 map15 = new Map15();
			map15.createWalls(world, camera);
			map15.setBackground(new Image(AssetsLoader.maps.get(14)));
			break;
		default:
			break;
		}

		if (!AssetsLoader.carfireSound.isPlaying()) {
			AssetsLoader.carfireSound.setVolume(0.1f);
			AssetsLoader.carfireSound.play();
		}
	}

	public static void resetCarVelocityEachTime() {
		TopDownCar.acc = false;
		TopDownCar.dec = false;
		TopDownCar.non = true;
		TopDownCar.steerR = false;
		TopDownCar.steerL = false;
		TopDownCar.steerN = true;

	}

	public static void createOuterWalls() {
		outerWalls = new OuterWalls();
		outerWalls.createOuterWalls(world, camera);
	}

	public static void createCar() {
		car = new TopDownCar();
		car.create(world, CarConfig.carWidth, CarConfig.carLength, CarConfig.carPosition, CarConfig.carAngle,
				CarConfig.carPower, CarConfig.carMaxSteerAngle, CarConfig.carMaxSpeed);
		CarConfig.carBody = car.getBaseCar().body;
	}

	public static void createInputs() {
		multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(car.getStage());
		Gdx.input.setInputProcessor(multiplexer);
	}

	public static void updateWorld(float delta) {

		stage.act(delta);
		stage.draw();
		car.getStage().act(delta);
		car.getStage().draw();

		world.step(delta, 3, 3);
		car.update(delta);
		camera.update();

	}

	public static void drawWorldSprites(float delta, SpriteBatch spriteBatch) {

		Array<Body> tmpBodies = new Array<Body>();
		world.getBodies(tmpBodies);

		spriteBatch.setProjectionMatrix(camera.combined);

		for (Body body : tmpBodies) {
			if (body.getUserData() != null && body.getUserData() instanceof Sprite) {
				Sprite sprite = (Sprite) body.getUserData();
				sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y
						- sprite.getHeight() / 2);
				sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
				spriteBatch.begin();
				sprite.draw(spriteBatch);
				spriteBatch.end();
			}
		}

	}

	public static void createWorldContactListener() {

		world.setContactListener(new ContactListener() {

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void beginContact(Contact contact) {
				Fixture fixtureA = contact.getFixtureA();
				Body bodyB = contact.getFixtureB().getBody();

				if (gameState == GameState.PLAYING) {
					checkIfCollided(fixtureA);
					checkCarIfParked(fixtureA, bodyB);
				}

			}

		});
	}

	private static void checkCarIfParked(Fixture fixtureA, Body bodyB) {

		if (bodyB.equals(CarConfig.carBody) && fixtureA.isSensor()) {
			unlockMap(getCurrentLevel());
			ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
			ScreenManager.getInstance().show(MyScreens.LEVEL_SCREEN);
		}
	}

	private static void checkIfCollided(Fixture fixtureA) {
		
		if (!fixtureA.isSensor()) {
			if (AssetsLoader.gamePlaySound.isPlaying()) {
				AssetsLoader.gamePlaySound.stop();
			}
			if (AssetsLoader.carfireSound.isPlaying()) {
				AssetsLoader.carfireSound.stop();
			}
			if (!AssetsLoader.carCrashSound.isPlaying()) {
				AssetsLoader.carCrashSound.setVolume(0.1f);
				AssetsLoader.carCrashSound.play();
			}
			gameState = GameState.GAMEOVER;
			showIntersititial();
			ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
			ScreenManager.getInstance().show(MyScreens.CRASH_SCREEN);
		}
	}

	private static void unlockMap(int currentLevel) {
		switch (currentLevel) {
		case 1:
			ParkingMania.getPrefs().saveMapLock(12 + "", true);
			break;
		case 2:
			ParkingMania.getPrefs().saveMapLock(13 + "", true);
			break;
		case 3:
			ParkingMania.getPrefs().saveMapLock(14 + "", true);
			break;
		case 4:
			ParkingMania.getPrefs().saveMapLock(15 + "", true);
			break;
		case 5:
			ParkingMania.getPrefs().saveMapLock(21 + "", true);
			break;
		case 6:
			ParkingMania.getPrefs().saveMapLock(22 + "", true);
			break;
		case 7:
			ParkingMania.getPrefs().saveMapLock(23 + "", true);
			break;
		case 8:
			ParkingMania.getPrefs().saveMapLock(24 + "", true);
			break;
		case 9:
			ParkingMania.getPrefs().saveMapLock(25 + "", true);
			break;
		case 10:
			ParkingMania.getPrefs().saveMapLock(31 + "", true);
			break;
		case 11:
			ParkingMania.getPrefs().saveMapLock(32 + "", true);
			break;
		case 12:
			ParkingMania.getPrefs().saveMapLock(33 + "", true);
			break;
		case 13:
			ParkingMania.getPrefs().saveMapLock(34 + "", true);
			break;
		case 14:
			ParkingMania.getPrefs().saveMapLock(35 + "", true);
			break;
		}

	}

	public static int getCurrentLevel() {
		return gameLevel;
	}

	public static void setCurrentLevel(int level) {
		gameLevel = level;
	}

}
