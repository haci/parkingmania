package com.mobaxe.parking.level2;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map8 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(12.6f, worldHeight - 12.8f), new Vector2(0.2f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 7.7f, 4f, new Vector2(-worldWidth + 24.5f,
				worldHeight - 17f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(3), 8, 4.3f));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(1.5f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 31.5f, worldHeight - 16.5f));
		circle2.setUserData(new CustomSprite(AssetsLoader.fountain, 3f, 3f));

		BoxProperties c2 = new BoxProperties(world, 3.7f, 6.7f, new Vector2(-worldWidth + 38.5f,
				worldHeight - 16.2f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(4), 4, 7f));

		BoxProperties c3 = new BoxProperties(world, 7.7f, 4.7f, new Vector2(-worldWidth + 36.5f,
				worldHeight - 23.2f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 8, 5));

		BoxProperties c4 = new BoxProperties(world, 3.7f, 3.7f, new Vector2(-worldWidth + 30.5f,
				worldHeight - 24f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4, 4));

		BoxProperties c5 = new BoxProperties(world, 3.7f, 3.7f, new Vector2(-worldWidth + 26.5f,
				worldHeight - 24f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4, 4));

		BoxProperties c6 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 20.9f,
				worldHeight - 27f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c7 = new BoxProperties(world, 3.7f, 3.7f, new Vector2(-worldWidth + 52.5f,
				worldHeight - 27f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4, 4));

		BoxProperties c8 = new BoxProperties(world, 3.7f, 3.7f, new Vector2(-worldWidth + 47.5f,
				worldHeight - 27f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4, 4));

		BoxProperties c9 = new BoxProperties(world, 3.7f, 3.7f, new Vector2(-worldWidth + 47.5f,
				worldHeight - 20f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4, 4));

		BoxProperties c10 = new BoxProperties(world, 3.7f, 3.7f, new Vector2(-worldWidth + 43.5f,
				worldHeight - 20f));
		c10.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4, 4));

		BoxProperties c11 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 52f,
				worldHeight - 13));
		c11.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c12 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 42.8f,
				worldHeight - 13));
		c12.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

	}
}
