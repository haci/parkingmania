package com.mobaxe.parking.level2;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map9 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(6.3f, worldHeight - 13f), new Vector2(0.5f, 1.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 7.7f, 4.5f, new Vector2(-worldWidth + 23.5f,
				worldHeight - 20f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 8, 4.8f));

		BoxProperties c2 = new BoxProperties(world, 7f, 3.5f, new Vector2(-worldWidth + 38f,
				worldHeight - 15.7f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.tripleCars, 7.5f, 3.8f));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(4.5f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 37f, worldHeight - 22f));
		circle2.setUserData(new CustomSprite(AssetsLoader.fountain, 9f, 9f));

		BoxProperties c3 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 31.5f,
				worldHeight - 25.5f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(2), 4, 2));

		BoxProperties c4 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 21.5f,
				worldHeight - 26.5f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c5 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 21.5f,
				worldHeight - 24f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c6 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 43.5f,
				worldHeight - 25.8f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c7 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 51f,
				worldHeight - 23.5f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(2), 4, 2));

		BoxProperties c8 = new BoxProperties(world, 5.7f, 3.7f, new Vector2(-worldWidth + 45f,
				worldHeight - 18f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(1), 6, 4));

	}
}
