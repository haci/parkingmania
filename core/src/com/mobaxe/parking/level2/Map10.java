package com.mobaxe.parking.level2;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map10 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(15.2f, worldHeight - 25f), new Vector2(1f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 8.7f, 4.7f, new Vector2(-worldWidth + 40f,
				worldHeight - 15f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 9, 5));

		BoxProperties c2 = new BoxProperties(world, 7.7f, 3.7f, new Vector2(-worldWidth + 49.5f,
				worldHeight - 27f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(3), 8, 4));

		BoxProperties c3 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 21.2f,
				worldHeight - 26.8f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c4 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 33f,
				worldHeight - 16.3f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(2), 4, 2));

		BoxProperties c5 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 26.3f,
				worldHeight - 22.8f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(3), 2, 4));

		BoxProperties c6 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 30f,
				worldHeight - 20.3f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c7 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 30f,
				worldHeight - 20.3f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c8 = new BoxProperties(world, 1.7f, 3.7f, new Vector2(-worldWidth + 32.5f,
				worldHeight - 27));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(4), 2, 4));

		BoxProperties c9 = new BoxProperties(world, 3.9f, 3.7f, new Vector2(-worldWidth + 48.5f,
				worldHeight - 23f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4.2f, 4f));

		BoxProperties c10 = new BoxProperties(world, 7f, 3.5f, new Vector2(-worldWidth + 49.5f,
				worldHeight - 15.5f));
		c10.getBody().setUserData(new CustomSprite(AssetsLoader.tripleCars, 7.5f, 3.8f));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(3f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 39f, worldHeight - 22f));
		circle2.setUserData(new CustomSprite(AssetsLoader.fountain, 6f, 6f));

	}
}
