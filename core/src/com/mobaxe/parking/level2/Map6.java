package com.mobaxe.parking.level2;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map6 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(-16.7f, worldHeight - 28f), new Vector2(0.5f, 1f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 7.7f, 4.5f, new Vector2(-worldWidth + 23.5f,
				worldHeight - 18f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 8, 4.8f));

		BoxProperties c2 = new BoxProperties(world, 3.9f, 3.7f, new Vector2(-worldWidth + 31.5f,
				worldHeight - 18f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.doubleCars, 4.2f, 4f));

		BoxProperties c3 = new BoxProperties(world, 7.7f, 4.5f, new Vector2(-worldWidth + 39.5f,
				worldHeight - 18f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 8, 4.8f));

		BoxProperties c4 = new BoxProperties(world, 6.6f, 3.7f, new Vector2(-worldWidth + 47.5f,
				worldHeight - 17f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.m6car2, 7f, 4f));

		BoxProperties c5 = new BoxProperties(world, 2.4f, 6.2f, new Vector2(-worldWidth + 47f,
				worldHeight - 22.8f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 2.7f, 6.5f));

		BoxProperties c6 = new BoxProperties(world, 2.4f, 6.2f, new Vector2(-worldWidth + 53f,
				worldHeight - 26f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 2.7f, 6.5f));

		BoxProperties c7 = new BoxProperties(world, 6.7f, 3.7f, new Vector2(-worldWidth + 23.5f,
				worldHeight - 23f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.m6car2, 7, 4));

		BoxProperties c8 = new BoxProperties(world, 5.2f, 3.7f, new Vector2(-worldWidth + 30.5f,
				worldHeight - 22.7f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(1), 6f, 4f));

		BoxProperties c9 = new BoxProperties(world, 3.2f, 1.5f, new Vector2(-worldWidth + 43.8f,
				worldHeight - 21.5f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 3.8f, 2f));

		// FOUNTAIN
		CircleShape cs = (CircleShape) Box2DFactory.createCircleShape(3f);
		FixtureDef fd = Box2DFactory.createFixture(cs, 1, 0.1f, 0.1f, false);
		Body circle = Box2DFactory.createBody(world, BodyType.StaticBody, fd, new Vector2(-worldWidth + 39f,
				worldHeight - 26.8f));
		circle.setUserData(new CustomSprite(AssetsLoader.fountain, 6f, 6f));

	}
}
