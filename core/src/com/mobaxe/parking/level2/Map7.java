package com.mobaxe.parking.level2;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mobaxe.parking.GameManager;
import com.mobaxe.parking.helpers.AssetsLoader;
import com.mobaxe.parking.helpers.Box2DFactory;
import com.mobaxe.parking.helpers.CustomSprite;
import com.mobaxe.parking.world.BoxProperties;
import com.mobaxe.parking.world.ParkingArea;

public class Map7 {

	public void setBackground(Image background) {
		GameManager.stage.addActor(background);
	}

	private void createParkingArea(World world, float worldHeight) {
		ParkingArea.create(world, new Vector2(16.4f, worldHeight - 20f), new Vector2(1f, 0.5f));
	}

	public void createWalls(World world, OrthographicCamera camera) {
		float worldWidth = camera.viewportWidth;
		float worldHeight = camera.viewportHeight;

		createParkingArea(world, worldHeight);

		BoxProperties c1 = new BoxProperties(world, 7.7f, 4.5f, new Vector2(-worldWidth + 23.5f,
				worldHeight - 18f));
		c1.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(0), 8, 4.8f));

		BoxProperties c2 = new BoxProperties(world, 5.7f, 3.7f, new Vector2(-worldWidth + 32.5f,
				worldHeight - 19f));
		c2.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(1), 6, 4));

		BoxProperties c3 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 32.5f,
				worldHeight - 13.2f));
		c3.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c4 = new BoxProperties(world, 7f, 3.5f, new Vector2(-worldWidth + 39.7f,
				worldHeight - 18f));
		c4.getBody().setUserData(new CustomSprite(AssetsLoader.tripleCars, 7.5f, 3.8f));

		BoxProperties c5 = new BoxProperties(world, 3.2f, 6.7f, new Vector2(-worldWidth + 53.5f,
				worldHeight - 16f));
		c5.getBody().setUserData(new CustomSprite(AssetsLoader.houses.get(2), 3.5f, 7f));

		BoxProperties c6 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 30f,
				worldHeight - 25.5f));
		c6.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		BoxProperties c7 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 35f,
				worldHeight - 25.5f));
		c7.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(2), 4, 2));

		BoxProperties c8 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 40f,
				worldHeight - 25.5f));
		c8.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(0), 4, 2));

		BoxProperties c9 = new BoxProperties(world, 3.7f, 1.7f, new Vector2(-worldWidth + 45f,
				worldHeight - 25.5f));
		c9.getBody().setUserData(new CustomSprite(AssetsLoader.cars.get(1), 4, 2));

		// FOUNTAIN
		CircleShape cs = (CircleShape) Box2DFactory.createCircleShape(2.8f);
		FixtureDef fd = Box2DFactory.createFixture(cs, 1, 0.1f, 0.1f, false);
		Body circle = Box2DFactory.createBody(world, BodyType.StaticBody, fd, new Vector2(-worldWidth + 49f,
				worldHeight - 21f));
		circle.setUserData(new CustomSprite(AssetsLoader.fountain, 6f, 6f));

		// FOUNTAIN
		CircleShape cs2 = (CircleShape) Box2DFactory.createCircleShape(1.5f);
		FixtureDef fd2 = Box2DFactory.createFixture(cs2, 1, 0.1f, 0.1f, false);
		Body circle2 = Box2DFactory.createBody(world, BodyType.StaticBody, fd2, new Vector2(
				-worldWidth + 25.3f, worldHeight - 25f));
		circle2.setUserData(new CustomSprite(AssetsLoader.fountain, 3f, 3f));

	}
}
